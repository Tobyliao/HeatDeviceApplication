package com.goldenfield.tobyliao.heatdeviceapplication;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goldenfield.tobyliao.heatdeviceapplication.SQLiteOpenHelper.WelcomePage;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import androidRecyclerView.Message;


public class MainActivity extends AppCompatActivity {

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;


    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
//    private EditText mOutEditText;
//    private Button mSendButton;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // String buffer for outgoing messages
//    private StringBuffer mOutStringBuffer;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    // Member object for the chat services
    private BluetoothChatService mChatService = null;




    public int counter = 0;


    private List<Message> messageList = new ArrayList<Message>();
    private TextView downCount;
    private ImageView playBTN;
    private ImageView timerBTN;
    private ImageView countClock;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        final ImageButton imgBTN55 = findViewById(R.id.imgBTN55);
        final ImageButton imgBTN50 = findViewById(R.id.imgBTN50);
        final ImageButton imgBTN45 = findViewById(R.id.imgBTN45);
        final ImageButton imgBTN40 = findViewById(R.id.imgBTN40);
        final ImageButton overheat = findViewById(R.id.overheat);
        final ImageView jump = findViewById(R.id.jump);


        final ImageView canvas = findViewById(R.id.canvas);
        final ImageView timerBTN10 = findViewById(R.id.timerBTN10);
        final ImageView timerBTN20 = findViewById(R.id.timerBTN20);
        final ImageView timerBTN30 = findViewById(R.id.timerBTN30);

        final TextView caution = findViewById(R.id.caution);
        final TextView textView55 = findViewById(R.id.textView);
        final TextView textView50 = findViewById(R.id.textView2);
        final TextView textView45 = findViewById(R.id.textView3);
        final TextView textView40 = findViewById(R.id.textView4);


        final TextView toClick55 = findViewById(R.id.ToClick55);
        final TextView toClick50 = findViewById(R.id.ToClick50);
        final TextView toClick45 = findViewById(R.id.ToClick45);
        final TextView toClick40 = findViewById(R.id.ToClick40);
        final TextView toClickOH = findViewById(R.id.toClickOH);





        downCount = findViewById(R.id.downCount);
        playBTN = findViewById(R.id.playBTN);
        timerBTN = findViewById(R.id.timerBTN);
        countClock = findViewById(R.id.countClock);

        caution.setVisibility(View.INVISIBLE);
        overheat.setVisibility(View.INVISIBLE);

        imgBTN55.setVisibility(View.INVISIBLE);
        imgBTN50.setVisibility(View.INVISIBLE);
        imgBTN45.setVisibility(View.INVISIBLE);
        imgBTN40.setVisibility(View.INVISIBLE);

        canvas.setVisibility(View.INVISIBLE);
        timerBTN10.setVisibility(View.INVISIBLE);
        timerBTN20.setVisibility(View.INVISIBLE);
        timerBTN30.setVisibility(View.INVISIBLE);

        countClock.setVisibility(View.INVISIBLE);
        playBTN.setVisibility(View.INVISIBLE);
        downCount.setVisibility(View.INVISIBLE);


        toClickOH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overheat.setVisibility(View.VISIBLE);
                overheat.setBackgroundColor(Color.TRANSPARENT);
                imgBTN55.setVisibility(View.INVISIBLE);
                imgBTN50.setVisibility(View.INVISIBLE);
                imgBTN45.setVisibility(View.INVISIBLE);
                imgBTN40.setVisibility(View.INVISIBLE);

                caution.setVisibility(View.VISIBLE);

                caution.setTextSize(14);
                textView55.setTextSize(14);
                textView50.setTextSize(14);
                textView45.setTextSize(14);
                textView40.setTextSize(14);

                sendMessage("T55");

                Toast toast = Toast.makeText(MainActivity.this, "BTN55", Toast.LENGTH_LONG);
                toast.show();


            }
        });

        toClick55.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgBTN55.setVisibility(View.VISIBLE);
                imgBTN55.setBackgroundColor(Color.TRANSPARENT);
                imgBTN50.setVisibility(View.INVISIBLE);
                imgBTN45.setVisibility(View.INVISIBLE);
                imgBTN40.setVisibility(View.INVISIBLE);

                textView55.setTextSize(20);
                textView50.setTextSize(14);
                textView45.setTextSize(14);
                textView40.setTextSize(14);

                caution.setVisibility(View.INVISIBLE);
                overheat.setVisibility(View.INVISIBLE);

                sendMessage("T55");

                Toast toast = Toast.makeText(MainActivity.this, "BTN55", Toast.LENGTH_LONG);
                toast.show();


            }
        });

        toClick50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgBTN50.setVisibility(View.VISIBLE);
                imgBTN50.setBackgroundColor(Color.TRANSPARENT);
                imgBTN55.setVisibility(View.INVISIBLE);
                imgBTN45.setVisibility(View.INVISIBLE);
                imgBTN40.setVisibility(View.INVISIBLE);

                textView50.setTextSize(20);
                textView55.setTextSize(14);
                textView45.setTextSize(14);
                textView40.setTextSize(14);

                caution.setVisibility(View.INVISIBLE);
                overheat.setVisibility(View.INVISIBLE);

                sendMessage("T50");

                Toast toast = Toast.makeText(MainActivity.this, "BTN50", Toast.LENGTH_LONG);
                toast.show();


            }
        });

        toClick45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgBTN45.setVisibility(View.VISIBLE);
                imgBTN45.setBackgroundColor(Color.TRANSPARENT);
                imgBTN50.setVisibility(View.INVISIBLE);
                imgBTN55.setVisibility(View.INVISIBLE);
                imgBTN40.setVisibility(View.INVISIBLE);

                textView45.setTextSize(20);
                textView50.setTextSize(14);
                textView55.setTextSize(14);
                textView40.setTextSize(14);

                caution.setVisibility(View.INVISIBLE);
                overheat.setVisibility(View.INVISIBLE);

                sendMessage("T45");

                Toast toast = Toast.makeText(MainActivity.this, "BTN45", Toast.LENGTH_LONG);
                toast.show();




            }
        });

        toClick40.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgBTN40.setVisibility(View.VISIBLE);
                imgBTN40.setBackgroundColor(Color.TRANSPARENT);
                imgBTN50.setVisibility(View.INVISIBLE);
                imgBTN45.setVisibility(View.INVISIBLE);
                imgBTN55.setVisibility(View.INVISIBLE);

                textView40.setTextSize(20);
                textView50.setTextSize(14);
                textView45.setTextSize(14);
                textView55.setTextSize(14);

                caution.setVisibility(View.INVISIBLE);
                overheat.setVisibility(View.INVISIBLE);

                sendMessage("T40");

                Toast toast = Toast.makeText(MainActivity.this, "BTN40", Toast.LENGTH_LONG);
                toast.show();

            }
        });

        timerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canvas.setVisibility(View.VISIBLE);
                timerBTN10.setVisibility(View.VISIBLE);
                timerBTN20.setVisibility(View.VISIBLE);
                timerBTN30.setVisibility(View.VISIBLE);

                timerBTN10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendMessage("t010");
                        canvas.setVisibility(View.INVISIBLE);
                        timerBTN10.setVisibility(View.INVISIBLE);
                        timerBTN20.setVisibility(View.INVISIBLE);
                        timerBTN30.setVisibility(View.INVISIBLE);
                        timerBTN.setVisibility(View.INVISIBLE);

                        playBTN.setVisibility(View.VISIBLE);
                        countClock.setVisibility(View.VISIBLE);
                        downCount.setVisibility(View.VISIBLE);

                        countDown(10*60*1000);

                    }
                });
                timerBTN20.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendMessage("t020");
                        canvas.setVisibility(View.INVISIBLE);
                        timerBTN10.setVisibility(View.INVISIBLE);
                        timerBTN20.setVisibility(View.INVISIBLE);
                        timerBTN30.setVisibility(View.INVISIBLE);
                        timerBTN.setVisibility(View.INVISIBLE);

                        playBTN.setVisibility(View.VISIBLE);
                        countClock.setVisibility(View.VISIBLE);
                        downCount.setVisibility(View.VISIBLE);
                        countDown(20*60*1000);

                    }
                });
                timerBTN30.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendMessage("t030");
                        canvas.setVisibility(View.INVISIBLE);
                        timerBTN10.setVisibility(View.INVISIBLE);
                        timerBTN20.setVisibility(View.INVISIBLE);
                        timerBTN30.setVisibility(View.INVISIBLE);
                        timerBTN.setVisibility(View.INVISIBLE);

                        playBTN.setVisibility(View.VISIBLE);
                        countClock.setVisibility(View.VISIBLE);
                        downCount.setVisibility(View.VISIBLE);
                        countDown(30*60*1000);

                    }
                });



            }


        });

        jump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WelcomePage.class);
                startActivity(intent);
            }
        });


        //start the bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //if the adapter is null, then the bluetooth is not supported
        if (mBluetoothAdapter == null){
            Toast toast = Toast.makeText(this, "The device is not supported the Blustooth", Toast.LENGTH_LONG);
            toast.show();
            finish();
            return;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null) setupChat(40);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                mChatService.start();
            }
        }
    }

    private void setupChat(int number) {
//        mOutEditText = (EditText) findViewById(R.id.edit_text_out);
//        mOutEditText.setOnEditorActionListener(mWriteListener);
//        mSendButton = (Button) findViewById(R.id.button_send);
        mChatService = new BluetoothChatService(this, mHandler);
        if (number == 55 ) sendMessage("T55");
        else if (number == 50 ) sendMessage("T50");
        else if (number == 45 ) sendMessage("T45");
        else if (number == 40 ) sendMessage("T40");

//        mSendButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                TextView view = (TextView) findViewById(R.id.edit_text_out);
//                String message = view.getText().toString();
//                sendMessage(message);
//            }
//        });

        // Initialize the BluetoothChatService to perform bluetooth connections


        // Initialize the buffer for outgoing messages
//        mOutStringBuffer = new StringBuffer("");
    }

    @Override
    public synchronized void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }


    private void sendMessage(String message) {


        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, "Bluetooth not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);
//            // Reset out string buffer to zero and clear the edit text field
//            mOutStringBuffer.setLength(0);
//            mOutEditText.setText(mOutStringBuffer);
        }
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    // If the action is a key-up event on the return key, send the message
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    return true;
                }
            };

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
//                    mAdapter.notifyDataSetChanged();
                    messageList.add(new androidRecyclerView.Message(counter++, writeMessage, "Me"));
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    mAdapter.notifyDataSetChanged();
                    messageList.add(new androidRecyclerView.Message(counter++, readMessage, mConnectedDeviceName));
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BluetoothDevice object
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    // Attempt to connect to the device
                    mChatService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat(40);
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, "Bluetooth Not Enable Leaving", Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    public void connect(View v) {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    public void discoverable(View v) {
        ensureDiscoverable();
    }

    public void countDown(int timeInterval){
        new CountDownTimer(timeInterval, 1000){

            @Override
            public void onTick(long l) {
                downCount.setText((l/1000)/60+":"+(l/1000)%60);
            }

            @Override
            public void onFinish() {
                playBTN.setVisibility(View.INVISIBLE);
                countClock.setVisibility(View.INVISIBLE);
                downCount.setVisibility(View.INVISIBLE);

                timerBTN.setVisibility(View.VISIBLE);
            }
        }.start();
    }
}
